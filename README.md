# AutoTrading
Trading app for business

# Create .env
```
API_KEY_BINANCE="<yourkeyAPI>"
API_SECRET_BINANCE="<yoursecretAPI>"
TRAINING_MODE = true
API_KEY_FINNHUB="<yoursecretAPI>"
```
  
# Documentation
- https://binance-docs.github.io/apidocs/spot/en/#introduction
- https://finnhub.io/docs/api/
