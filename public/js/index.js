var date = new Date();
var year = date.getFullYear();
var month = date.getMonth()+1;
var day = date.getDate();
var endDate = year+'-'+month+'-'+day;
var StartDate = (year-1)+'-'+month+'-'+day;

// Otherwise, selectors are also supported
flatpickr("#date1", {
    dateFormat: "Y-m-d",
    minDate: StartDate,
    defaultDate: [StartDate]
});
flatpickr("#date2", {
    dateFormat: "Y-m-d",
    minDate: StartDate,
    defaultDate: [endDate]
});
// creates multiple instances
flatpickr(".anotherSelector");

var baseCurr = 'USD';
CallGetConvert(baseCurr);
$('#selected_left').on('change', function(e) {
    var selected = $('#selected_left').find(":selected").text();
    baseCurr = selected;
    CallGetConvert(baseCurr);
    console.log("baseCurr : "+baseCurr);
});

function CallGetConvert(baseCurr){
    $.ajax({
        url : 'http://127.0.0.1:8080/finnhub/convert'+baseCurr, // La ressource ciblée
        type : 'GET', // Le type de la requête HTTP.
        
        success: function(data){
            $('#selected_right').children().remove().end()
            var count = Object.keys(data.message.quote).length;
            for(var i=0;i<count;i++){
                //Input right convert from baseCurr // CODE + VALUE
                $("#selected_right").append(new Option(Object.entries(data.message.quote)[i][0],Object.entries(data.message.quote)[i][1]));

                //Input left convert Symbols // Value -> 1
                $("#selected_left").append(new Option(Object.entries(data.message.quote)[i][0],"1"));
            }
            $("#input_right").val($("#selected_right").val());
        },
        error: function(e) {
            console.log(e);
        }
    });
}

$(document).ready(function() {
    var precision = $("#Precision").val();
    var symbolName = $("#Symbols").val();
    var date1 = $("#date1").val();
    var date2 = $("#date2").val();
    DisplayVolats(date1,date2,precision,symbolName);
});

$('#ApplyFilters').on('click', function(e) {
    var precision = $("#Precision").val();
    var symbolName = $("#Symbols").val();
    var date1 = $("#date1").val();
    var date2 = $("#date2").val();
    FillChart(date1,date2,precision,symbolName);
});

$('#selected_right').on('change', function(e) {
    $("#input_right").val($("#selected_right").val());
});
