var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Volatilité (%)',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

function clear(){
    console.log()
    myChart.data.labels = [];
    myChart.data.datasets[0].data = [];
    myChart.clear();
    myChart.update();
}

function VolMoyenne(){
    myChart.update();
    var datas = myChart.data.datasets[0].data;
    var VolatFinalGood = 0;
    for(var k=0;k<datas.length;k++){
      if(parseFloat(datas[k]) > 0){
        VolatFinalGood += parseFloat(datas[k]);
      }else{
        VolatFinalGood -= parseFloat(datas[k]);
      }
    }
    return parseFloat(VolatFinalGood/datas.length).toFixed(2).toString(); 
}

function FillSymbols(){
    $.ajax({
        url : 'http://127.0.0.1:8080/binance/symbols', // La ressource ciblée
        type : 'GET', // Le type de la requête HTTP.
        success: function(data){
            $('#Symbols').children().remove().end();
            var symbols = Object.entries(data.message);
            for(var i=0;i<symbols.length;i++){
                var finalDisplay = "";
                var display = symbols[i][1].split('USDT');
                finalDisplay = display[0]+"/USDT";
                $("#Symbols").append(new Option(finalDisplay,symbols[i][1]));
            }
        },
        error: function(e) {
            console.log(e);
        }
    })
}


function DisplayVolats(date1,date2,precision,symbolName){
    var symbs = [];
    new Promise((resolve, reject)=>{
        $.ajax({
            url : 'http://127.0.0.1:8080/binance/symbols', // La ressource ciblée
            type : 'GET', // Le type de la requête HTTP.
            success: function(data){
                resolve(data.message)
            },
            error: function(e) {
                console.log(e);
            }
        }).then((data)=>{
            if(data){
                var ObjToArray = Object.entries(data.message);
                for(var i=0;i<ObjToArray.length;i++){
                    symbs.push(ObjToArray[i][1]);
                }
            }
        })
    }).then(async (symbs)=>{
        for(var i=0;i<symbs.length;i++){
            var SymbolName = symbs[i];
            var datatosend = {
                startTimeBody : date1,
                endTimeBody : date2,
                precisionBody : precision,
                symbolName : SymbolName,
            }
            await new Promise((resolve, reject)=>{
                $.ajax({
                    url : 'http://127.0.0.1:8080/binance/vol2', // La ressource ciblée
                    type : 'POST', // Le type de la requête HTTP.
                    data: datatosend,
                    success: function(dataR){
                        resolve(dataR)
                    },
                    error: function(e) {
                        console.log(e);
                    }
                }).then((data2)=>{
                    //console.log(data2)
                    var recup = VolMoyenneDynamics(data2,SymbolName);
                    console.log(recup[0]+" "+recup[1]);
                    jQuery("<tr id=_"+recup[1]+"></tr>").appendTo("#tableVolatBody");
                    jQuery("<th scope='row' id=_th"+recup[1]+">"+recup[1]+"</th>").appendTo("#_"+recup[1]);
                    jQuery("<td id=_td"+recup[0]+">"+(parseFloat(recup[0]).toFixed(2)).toString()+"%"+"</td>").appendTo("#_"+recup[1]);
                })
            }) 
        }
    }) 
    
}

function VolMoyenneDynamics(datas,symbolName){
    var VolatFinalGood = 0;
    
    for(var k=0;k<datas.length;k++){
        
      if(parseFloat(datas[k]) > 0){
        VolatFinalGood += parseFloat(datas[k]);
      }else{
        VolatFinalGood -= parseFloat(datas[k]);
      }
    }
    return [parseFloat(VolatFinalGood/datas.length).toString()+"%",symbolName]; 
}


FillSymbols();
function FillChart(date1,date2,precision,symbolName){
    var datatosend = {
        startTimeBody : date1,
        endTimeBody : date2,
        precisionBody : precision,
        symbolName : symbolName
    }
    clear();
    $.ajax({
        url : 'http://127.0.0.1:8080/binance/vol2', // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data: datatosend,
        success: function(dataR){
            for(var i=0;i<dataR.length;i++){
                myChart.data.datasets[0].data[i] = dataR[i][0];
                myChart.data.labels[i] = dataR[i][1];
            }
            myChart.update();
        },
        error: function(e) {
            console.log(e);
        }
    }).then(()=>{
        //Call Vol Moyenne
        $("#volMoyenne").val(parseFloat(VolMoyenne()[0]).toFixed(2).toString()+"%");
    })

}