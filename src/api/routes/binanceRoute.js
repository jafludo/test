module.exports = (app,trainingMode) => {
    
    const binanceController = require('../controllers/binanceController');

    app.route('/binance')
    .get(binanceController.checkPrice)

    app.route('/binance/vol')
    .post(binanceController.storageSymbolDatas)

    app.route('/binance/vol2')
    .post(binanceController.displaysVolatilite)

    app.route('/binance/vol3')
    .get(binanceController.testexec)

    app.route('/binance/symbols')
    .get(binanceController.getAllSymbolsResp)

    app.route('/binance/orders')
    .get(binanceController.getOpenOrders)

    app.route('/binance/getbalances')
    .get(binanceController.getBalances)

    app.route('/binance/getdatas')
    .get(binanceController.getdatas)


}