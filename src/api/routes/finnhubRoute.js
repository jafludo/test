module.exports = (app,trainingMode) => {
    
    const finnhubController = require('../controllers/finnhubController');

    app.route('/finnhub')
    .get(finnhubController.test)

    app.route('/finnhub/convert:baseSymbol')
    .get(finnhubController.conversion_Symbols)

    app.route('/finnhub/test')
    .get(finnhubController.testwss)

    app.route('/finnhub/getdatas')
    .post(finnhubController.GetDatasFromSymbol)


}