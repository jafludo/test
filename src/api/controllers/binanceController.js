const Binance = require('node-binance-api');
const Candle = require('../models/binanceCandleModel');
const Cryptos = require('../models/binanceCryptos');
const dotenv = require('dotenv');
dotenv.config();

const Apikey = process.env.API_KEY_BINANCE; 
const Apisecretkey = process.env.API_SECRET_BINANCE;
const TrainingMode = process.env.TRAINING_MODE;

const binance = new Binance().options({
    APIKEY: Apikey,
    APISECRET: Apisecretkey,
    useServerTime: true,
    recvWindow: 60000, //Response Timeout
    verbose: true, //Mode Debug WebSocket
    log: log => {
      console.log(log); //Own logger for debug
    }
});


exports.test = async (req, res) => {
/**
 * TODO : Implement params symbol
*/

  let symbol = 'BTCUSDT';
  let ticker = await binance.prices();

  //var long = Object.keys(ticker).length;

  res.status(200);
  res.json({message: ticker[symbol]})  

};

exports.checkPrice = async (req, res) => {
  var convert = 'USDT';
  await new Promise(async (resolve, reject) => {
    let ticker = await binance.prices();
    resolve(ticker);
  }).then(async(tikers)=>{

    var symbolsCurPrice = Object.entries(tikers);
    var Goods = [];
    for(var i=0;i<symbolsCurPrice.length;i++){
      if(symbolsCurPrice[i][0].includes(convert) == true){
        Goods.push([symbolsCurPrice[i][0],symbolsCurPrice[i][1]])
      }
    }
    res.status(200);
    res.json({message: Goods})  
  })
  
};


/**
 * Function for store Orders on BDD
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 */
exports.storageOrders = async (req, res) => {
    // let symbol = 'BTCUSDT';
    // let ticker = await binance.prices();
  
    // res.status(200);
    // res.json({message: ticker[symbol]})  
    console.log("Test")
};


/**
 * Function for placing Orders on market
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 */
exports.placeOrders = async (req, res) => {
    let symbol = 'BTCUSDT';
    let ticker = await binance.prices();
  
    res.status(200);
    res.json({message: ticker[symbol]})  
};


exports.testexec = async (req, res) => {
  await new Promise((resolve, reject) => {
    var symbols = this.getAllSymbols(req, res);
    resolve(symbols);
  }).then(async (symbols)=>{
    for(var i=0;i<symbols.length;i++){
      await new Promise((resolve, reject)=>{
        setInterval(()=>{
          resolve(symbols[i])
        },1000)
      }).then((val)=>{
        this.storageSymbolDatas(req,res,val);
      })
    }   
    res.status(200);
    res.json({message: "done"})  
  })
};


exports.console = async (req, res,value) => {
  console.log(value)
};
exports.getAllSymbols = async (req, res) => {
  let ticker = await binance.prices();
  var Wait = [];
  var Goods = [];
  for(var i=0;i<Object.entries(ticker).length;i++){
    Wait.push(Object.entries(ticker)[i][0]);
  }
  for(var j=0;j<Wait.length;j++){
    if(Wait[j].includes('USDT') == true){
      Goods.push(Wait[j].toString());
    }
  }
  return Goods;
}

exports.getAllSymbolsResp = async (req, res) => {
  let ticker = await binance.prices();
  var Wait = [];
  var Goods = [];
  for(var i=0;i<Object.entries(ticker).length;i++){
    Wait.push(Object.entries(ticker)[i][0]);
  }
  for(var j=0;j<Wait.length;j++){
    if(Wait[j].includes('USDT') == true){
      Goods.push(Wait[j].toString());
    }
  }
  res.status(200);
  res.json({message: Goods})  
}

exports.StorageCryptosSymbols = async (req, res) => {
  let ticker = await binance.prices();
  var convert = 'USDT';
  var Wait = [];
  var Goods = [];
  await new Promise((resolve, reject) => {
    for(var i=0;i<Object.entries(ticker).length;i++){
      Wait.push(Object.entries(ticker)[i][0]);
    }
    for(var j=0;j<Wait.length;j++){
      if(Wait[j].includes(convert) == true){
        Goods.push(Wait[j]);
      }
    }
    resolve(Goods)
  }).then(async (Goods)=>{
    for(var k=0;k<Goods.length;k++){
      await new Promise((resolve2, reject) =>{
        Cryptos.exists({name: Goods[k]}, function (err, doc2) {
          if(err){
            console.log("error :"+e);
          }else{
            if(doc2 === false){
              resolve2('good');
            }else{
              resolve2('nogood');
            }
          }
        });
      }).then((value3)=>{
        if(value3 == 'good'){
          var new_crypto = new Cryptos({
            name: Goods[k],
            convert: convert
          });
          new_crypto.save();
          
        }else if(value3 == 'nogood'){

        }
      })
    }
  })
  res.status(200);
  res.json({message: Goods})  
}


exports.getdatas = async (req, res) => {
/**
 * TODO : Implement params symbol
*/

  //Specific symbol cz if false then response all symbols
  var symbol = 'BTCUSDT';

  binance.prevDay(symbol, (error, response) => {
    res.status(200);
    res.json({message: response})  
  });

};


/**
 * Function for getting currents balances > 0
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
*/
exports.getBalances =  (req, res) => {
  
  binance.balance((error, balances) => {
    if(error){
      res.status(500);
      res.json({message: "error : "+error})  
    }else{
      var Mbalances = [];
      var accounts_balances = Object.entries(balances);
      for(var i=0;i<accounts_balances.length;i++){
        var available = accounts_balances[i][1]['available'];
        if(available != 0){
          Mbalances.push([accounts_balances[i][0],available]);
        }
      } 

      //binance.buy("BTCUSDT", 1, 45000);
      //binance.buy("DOGEUSDT", 300, 0.011903);
      
      //binance.marketBuy("DOGEUSDT", quantity);
      /*
      binance.buy("DOGEUSDT", 300, 0.048, {type:'LIMIT'}, (error, response) => {
        console.info("Limit Buy response", response);
        console.info("order id: " + response.orderId);
      });
      */
      //binance.marketBuy("BNBBTC", quantity);
      res.status(200);
      res.json({message: Mbalances})  
      
    }
     
  });

};


/**
 * Function for register BDD Crypto Symbol since 12 months
 * 
 * @param {Request} req 
 * @param {Repsonse} res
 * 
*/
exports.storageSymbolDatas = async (req, res,symbolNameParams) => {

  //Day - 1 / Year - 1
  const endTime = Date.now() - 86400;
  const startTime = endTime - 31556926000;
  var symbolName = 'BTCUSDT';
  if(symbolNameParams){
    symbolName = symbolNameParams.toString();
  }
  console.log(symbolName);
  //Get 12 mois glissants depuis jour - 1
  var recup = new Promise((resolve,reject) =>{
    binance.candlesticks(symbolName, "1d", (error, ticks, symbol) => {
      //let last_tick = ticks[ticks.length - 1];
      //let [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = last_tick;
      resolve(ticks);
    }, {limit: 365, startTime : startTime, endTime: endTime});
  })
  recup.then(async (value) => {
    
    //Stockage BDD then recup vals
    for(var i=0;i<value.length;i++){
      var timeopenA = value[i][0].toString();
      var openprice = value[i][1];
      var highprice = value[i][2];
      var lowprice = value[i][3];
      var closeprice = value[i][4];
      var volume = value[i][5];
      var closeTime = value[i][6];
      var assetVolume = value[i][7];
      var trades = value[i][8];
      var buyBaseVolume = value[i][9];
      var buyAssetVolume = value[i][10];
      await new Promise((resolve, reject) => {
          Candle.exists({timeopen: timeopenA,name: symbolName}, async function (err, doc) { 
            if(err){ 
              console.log("error check candle : "+err);
            }else{ 
              //Si Candle don't exist go save
              if(doc === false){ 
                var volat = await calculVolatilite(openprice,closeprice);
                resolve({
                  name: symbolName,
                  timeopen: timeopenA,
                  openprice: openprice,
                  highprice: highprice,
                  lowprice: lowprice,
                  closeprice: closeprice,
                  volume: volume,
                  closeTime: closeTime,
                  assetVolume: assetVolume,
                  trades: trades,
                  buyBaseVolume: buyBaseVolume,
                  buyAssetVolume: buyAssetVolume,
                  volatilite: volat
                })    
              }else{
                resolve(true);       
              }
            } 
          })
        }).then((value)=>{
          if(value != true){
            var new_candle = new Candle(value);
            new_candle.save();
          }
        })
      }

    return 0;
  })

}

function calculVolatilite(openprice,closeprice){
  var dif = closeprice - openprice;
  var vol = ((Math.abs(dif)/openprice)*100).toFixed(2);
  var signal = null;
  if(dif >= 0){
    signal = "+";
  }else{
    signal = "-";
  }
  var res = signal+vol+"%";
  return res;
}


/**
 * Function Get All Orders Open on market
 * 
 * @param {Request} req 
 * @param {Repsonse} res
 * 
*/
exports.getOpenOrders = (req, res) => {

  binance.openOrders(false, (error, openOrders) => {
    res.status(200);
    res.json({message: openOrders})  
  });
}

exports.displaysVolatilite = (req, resp) => {

  var {startTimeBody} = req.body;
  var {endTimeBody} = req.body;
  var {precisionBody} = req.body;
  var {symbolName} = req.body;
  var endTime = null;
  var startTime = null;
  var precision = null;
  if(startTimeBody == undefined && endTimeBody == undefined && precisionBody == undefined){
    //Day - 1 / Year - 1
    endTime = Date.now() - 86400;
    startTime = endTime - 31556926000;
    precision = "Jour";
  }else{
    var startTimeToTimestamp = new Date(startTimeBody).getTime();
    var endTimeToTimestamp = new Date(endTimeBody).getTime();
    endTime = endTimeToTimestamp;
    startTime = startTimeToTimestamp;
    precision = precisionBody;
  }
  Candle.find({
    timeopen: {
      $gte: parseInt(startTime)
    },
    closeTime: {
      $lte: parseInt(endTime)
    },
    name: {
      $eq: symbolName
    },
  },(err,res)=>{
    var VolatFinal = [];
    for(var i=0,j=0;i<res.length;i++,j++){
      var volat = res[i].volatilite.split('%')[0];
      var timeopenS = res[i].timeopen;
      var DateFormattedFinal = "";
      var DateFormatted = new Date(timeopenS);
      var day = DateFormatted.getDate();
      var month = DateFormatted.getMonth();
      var year = DateFormatted.getFullYear();
      DateFormattedFinal = day+'/'+month+'/'+year;

      var signalvolat = volat.charAt(0);
      var tempvolat = volat.split(signalvolat)[1];
      var tempvolatprecision = 0;
      if(signalvolat == '+'){
        tempvolatprecision+=parseFloat(tempvolat);
      }else{
        tempvolatprecision-=parseFloat(tempvolat);
      }
      if(precision == "Jour"){
        if(tempvolatprecision >= 0){
          VolatFinal.push(['+'+tempvolatprecision.toString(),DateFormattedFinal]);
        }else{
          VolatFinal.push([tempvolatprecision.toString(),DateFormattedFinal]);
        }
        tempvolatprecision = 0;
      }else if(precision == "Semaine" && j >= 7){
        if(tempvolatprecision >= 0){
          VolatFinal.push(['+'+tempvolatprecision.toString(),DateFormattedFinal]);
        }else{
          VolatFinal.push([tempvolatprecision.toString(),DateFormattedFinal]);
        }
        tempvolatprecision = 0;
        j = 0;
      }else if(precision == "Mois" && j >= 30){
        if(tempvolatprecision >= 0){
          VolatFinal.push(['+'+tempvolatprecision.toString(),DateFormattedFinal]);
        }else{
          VolatFinal.push([tempvolatprecision.toString(),DateFormattedFinal]);
        }
        tempvolatprecision = 0;
        j = 0;
      }
      
    }
    
    resp.status(200);
    resp.json(VolatFinal)  

  })
};
