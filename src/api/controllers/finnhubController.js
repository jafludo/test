const dotenv = require('dotenv');
dotenv.config();
const request = require('request');
const tokenfinnhub = process.env.API_KEY_FINNHUB;

exports.test = (req, res) => {
    request('https://finnhub.io/api/v1/stock/profile2?token='+tokenfinnhub+'&symbol=AAPL', { json: true }, (err, result, body) => {
        if (err){ 
            res.status(500);
            res.json({message: "Erreur serveur."+err})
        }else{
            res.status(200);
            res.json({message: result.body})
        }

    });
};


exports.conversion_Symbols = (req, res) => {
    const baseSymbol = req.params.baseSymbol;
    request('https://finnhub.io/api/v1/forex/rates?token='+tokenfinnhub+'&base='+baseSymbol, { json: true }, (err, result, body) => {
        if (err){ 
            res.status(500);
            res.json({message: "Erreur serveur."+err})
        }else{
            res.status(200);
            res.json({message: result.body})
        }

    });
};

exports.GetDatasFromSymbol = (req, res) => {
    const {baseSymbol} = req.body;
    console.log("baseSymbol : "+baseSymbol);
    request('https://finnhub.io/api/v1/quote?token='+tokenfinnhub+'&symbol='+baseSymbol, { json: true }, (err, result, body) => {
        if (err){ 
            res.status(500);
            res.json({message: "Erreur serveur."+err})
        }else{
            res.status(200);
            var OpenPriceDay = result.body.o;
            var HighPriceDay = result.body.h;
            var LowPriceDay = result.body.l;
            var CurrentPriceDay = result.body.c;
            var PreviousClosePrice = result.body.pc;
            res.json({message: result.body})
        }
    });

};

exports.testwss = (req, res) => {
    var WebSocket = require('websocket').w3cwebsocket;
    const socket = new WebSocket('wss://ws.finnhub.io?token='+tokenfinnhub);

    // Connection opened -> Subscribe
    socket.addEventListener('open', function (event) {
        console.log("d")
        socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'AAPL'}))
        socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'BINANCE:BTCUSDT'}))
        socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'IC MARKETS:1'}))
    });
    
    // Listen for messages
    socket.addEventListener('message', function (event) {
        console.log('Message from server ', event.data);
    });
    
    // Unsubscribe
    var unsubscribe = function(symbol) {
        socket.send(JSON.stringify({'type':'unsubscribe','symbol': symbol}))
    }
};