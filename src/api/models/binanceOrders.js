const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let binanceOrders = new Schema({
  convertSymbol: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Orders', binanceOrders);