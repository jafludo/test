const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AllCryptos = new Schema({
  name: {
    type: String,
    required: true,
  },
  convert: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Cryptos', AllCryptos);