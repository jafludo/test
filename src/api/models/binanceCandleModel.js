const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CandleStock = new Schema({
  name: {
    type: String,
    required: true,
  },
  timeopen: {
    type: Number,
    required: true,
  },
  openprice: {
    type: String,
    required: true,
  },
  highprice: {
    type: String,
    required: true,
  },
  lowprice: {
    type: String,
    required: true,
  },
  closeprice: {
    type: String,
    required: true,
  },
  volume: {
    type: String,
    required: true,
  },
  closeTime: {
    type: Number,
    required: true,
  },
  assetVolume: {
    type: String,
    required: true,
  },
  trades: {
    type: String,
    required: true,
  },
  buyBaseVolume: {
    type: String,
    required: true,
  },
  buyAssetVolume: {
    type: String,
    required: true,
  },
  volatilite: {
    type: String,
  },
});

module.exports = mongoose.model('Candle', CandleStock);