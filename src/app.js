const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');  
const dotenv = require('dotenv');
const app = express();
const http = require('http');
const mongoose = require('mongoose');

//Server Web HTTP
var server = http.createServer(app).listen("8080", function(){
 
});

mongoose.connect('mongodb://localhost/auto_trading', {useNewUrlParser: true,useUnifiedTopology: true });
mongoose.set('useCreateIndex', true);

dotenv.config();
const trainingMode = process.env.TRAINING_MODE;

app.use(cors({credentials: true, origin: 'http://127.0.0.1:8080'}));

app.use(bodyParser.urlencoded({
  extended: true
}));


app.use(bodyParser.json());
app.use(express.static('../public'));

const indexRoute = require('./api/routes/indexRoute');
indexRoute(app,trainingMode);

const finnhubRoute = require('./api/routes/finnhubRoute');
finnhubRoute(app,trainingMode);

const binanceRoute = require('./api/routes/binanceRoute');
binanceRoute(app,trainingMode);